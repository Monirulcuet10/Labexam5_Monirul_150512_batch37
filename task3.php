<?php
class MyCalculator {

    private $_firstvalue, $_secondvalue;


    public function __construct( $_firstvalue,$_secondvalue ) {
        $this->_firstvalue = $_firstvalue;
        $this->_secondvalue =$_secondvalue;
    }
    public function add() {
        return $this->_firstvalue + $this->_secondvalue;
    }
    public function subtract() {
        return $this->_firstvalue - $this->_secondvalue;
    }
    public function multiply() {
        return $this->_firstvalue * $this->_secondvalue;
    }
    public function divide() {
        return $this->_firstvalue / $this->_secondvalue;
    }
}
$mycalc = new MyCalculator(12, 6);
echo $mycalc-> add()."::it is the result of add"."<br>";
echo $mycalc-> multiply()."::it is the result of mul"."<br>";
echo $mycalc->divide()."::it is the result of div"."<br>";
echo $mycalc->subtract()."::it is the result of sub"."<br>";
?>
